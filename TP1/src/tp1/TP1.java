/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tp1;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author loperret2
 */
public class TP1 {

    /**
     * @param args the command line arguments
     */
    
    private static List<Object> maListe = new LinkedList<>();
    
    public static void main(String[] args) {
        System.out.println("Goodbye world !");
        
        int i=0;
        while(true){
            try{
                Thread.sleep(20);
            }
            catch(InterruptedException e){
                
            }
            System.out.println("Iteration : " + i);
            i++;
            
            if(i == 500){
                for(int a=0; a<1600000; a++){
                    maListe.add(new Object());
                }
            }
            
            if(i == 650){
                maListe.clear();
                System.gc();
            }
            
            if(i == 900){
                for(int a=0; a<13000000; a++){
                    maListe.add(new Object());
                }
            }
            
            if(i == 1050){
                for(int a=0; a<1100000; a++){
                    maListe.add(new Object());
                }
            }
            
            if(i == 1090){
                maListe.clear();
                System.gc();
            }
        
            //exo1();
            //exo2();
            //exo3();
        }
    }
    
    public static void exo1(){
        List<Object> liste = new LinkedList<>();
        for(int i=0; i<100000; i++){
            liste.add(new Object());
        }
    }
    
    public static void exo2(){
        for(int i=0; i<100000; i++){
            maListe.add(new Object());
        }
    }
    
    public static void exo3(){
        for(int i = 0; i< 100000; i++){
            maListe.add(new Object());
        }
        for(int i = 0; i< 100; i++){
            maListe.remove(0);
        }
        
        System.gc();
    }
    
}
